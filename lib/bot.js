const Promise   = require('bluebird')
const Twit      = require('twit')
const bittrex   = require('node-bittrex-api')
const Lazy      = require('lazy.js')
const clone     = require('clone')
const Tesseract = require('tesseract.js')

const currencies = require('./currencies')

module.exports = class Bot {

  constructor(opts) {
    this.verbose = true
    this.btcSpend = 0.0541
    this.adjustment = .10
    this.market_bought = ''
    this.rate_bought = 99999
    this.quantity_bought = 99999
    this.sell_factor_high = 2.2
    this.sell_factor_low = 0.65
    //this.target_coin_balance = 0
    this.bal = 0
    this.imgText
    this.T = new Twit({
      consumer_key:        opts.twitter.key,
      consumer_secret:     opts.twitter.secret,
      access_token:        opts.twitter.token,
      access_token_secret: opts.twitter.tokenSecret
    })

    this.bittrex = Promise.promisifyAll(clone(bittrex))
    this.Tesseract = Promise.promisifyAll(clone(Tesseract))
    this.bittrex.options({
      apikey:                     opts.bittrex.key,
      apisecret:                  opts.bittrex.secret,
      inverse_callback_arguments: true
    })

    this.mcafeeId = '961445378' 
    //this.mcafeeId = '1546843350' //@breich90

    this.follow = [
      // @officialmcafee
      '961445378'
      // @mbb777_ (dummy account for testing tweets)
      //,'944877775705464832'
      // @breich90
      //'1546843350'
    ]
  }

  /**
   * Perform the asynchronous part of bot initialization and start listening for tweets.
   */
  async init() {
    // Twitter
    // https://stackoverflow.com/questions/15652361/can-not-use-follow-in-the-streaming-api-in-ntwitter-recieving-unspecified-er
    // https://stackoverflow.com/questions/47384200/how-to-stream-tweets-of-one-user-using-follow
    this.stream = this.T.stream('statuses/filter', { follow: this.follow })
    this.stream.on('tweet', this.handleTweet.bind(this))
    this.stream.on('connect', () => console.warn('connect'))
    this.stream.on('error', (err) => { console.warn('error', err) })
  }

  saveResult(result){
    this.imgText = result
  }

  /**
   * Analyze a tweet and try to buy if it's a coin of the day announcement.
   *
   * @param {Object} tweet          tweet data + metadata
   */
  async handleTweet(tweet) {
    if (this.verbose) console.warn(`${tweet.created_at} | ${tweet.user.screen_name} ${tweet.user.id_str} | ${tweet.text}`)
    if (this.isCoinOfTheDay(tweet)) {
      const coin = this.identifyCoin(tweet)
      console.warn('COIN OF THE DAY!', tweet.text, coin)
      if (coin.found) {
        await this.attemptBuy(coin)
        //await this.successfulBuy(res, coin)
        //console.warn(res)
        //console.warn(mon)
      } else {
        console.warn(coin)
      }
    }
  }

  async attemptBuy(coin){
    var res = await this.buy(coin.symbol, this.btcSpend, this.adjustment)
    console.warn(`attempting to buy ${coin.symbol}`)
    //await this.sleep()
    //await this.balance(coin.symbol)
    //console.warn(`current balance: ${this.bal}`)
    //while (this.bal==0){
    //  console.warn(`attempting to buy ${coin.symbol}`)
    //  res = await this.buy(coin.symbol, this.btcSpend, this.adjustment)
    //  await this.sleep()
    //  await this.balance(coin.symbol)
    //}
    this.successfulBuy(res, coin)
  }

  async successfulBuy(res, coin){
    //if coin buy is successful, subscribe to the coin and sell at 2x the purchase price
    this.market_bought = res.result.MarketName
    console.warn(this.market_bought)
    this.rate_bought = res.result.Rate
    console.warn(this.rate_bought)
    this.quantity_bought = res.result.Quantity
    console.warn(this.quantity_bought)
    console.warn('buy attempt succeeded!')
    let mon = await this.monitorCoin(coin.symbol, this.rate_bought, this.quantity_bought)
  }

/*   async balance(symbol){
    this.bal = await this.bittrex.getbalances(function( data, err ) {
      console.warn(`err: ${err.result}`)
      console.warn(`data: ${data}`)
      for (var i in err.result){
        //console.warn(err.result[i])
        if (err.result[i].Currency == symbol){
          console.warn(err.result[i].Balance)
          return err.result[i].Balance
        }
      }
      //while (err==null){}
      //this.target_coin_balance = data.result.Balance
      //console.warn(`target coin balance: ${this.target_coin_balance}`)
      //this.bal = parseFloat(err.result.balance)
    });
    console.warn(`current currency balance: ${this.bal}`)
  } */

  async sleep(){
    return new Promise(resolve=>{
      setTimeout(resolve,1000)
     })
 }

  async monitorCoin(symbol, r, q) {
    const market   = `BTC-${symbol}`
    const res0     = await this.bittrex.gettickerAsync({ market })
    const ticker   = res0.result
    const rate     = ticker.Bid 
    //if current market's rate is >= 2x the purchase rate, execute a sell, else recursively call
    //monitorCoin to check the rate again
    console.warn(`purchase rate was ${r}, current selling rate is ${rate}`)
    if (r*this.sell_factor_high <= rate || r*this.sell_factor_low > rate) {
      if (r*this.sell_factor_high <= rate) {
        console.warn(`rate is ${this.sell_factor_high} or greater than purchase price, executing sell!`)
      } else {
        console.warn(`rate is ${this.sell_factor_low} or lower than purchase price, executing sell!`)
      }
      let res1 = await this.sell(market, this.quantity_bought, this.adjustment)
      //await this.sleep()
      //let bal = await this.balance(symbol)
      //if sell is successful, return the message, else recursively call monitorCoin
/*       if (bal==0){  
        return res1
      } else {
        console.warn('sell unsuccessful, calling monitorCoin again')
        while (bal>0){
          let res1 = await this.sell(market, this.quantity_bought, this.adjustment)
          await this.sleep()
          let bal = await this.balance(symbol)
        }
        return res1
      } */
    } else {
      //console.warn(`rate is only at ${rate}, recursively calling monitorCoin`)
      let mon = await this.monitorCoin(symbol, r, q)
      return mon
    } 
  }

  /**
   * Is this tweet a coin of the day announcement from officialmcafee
   * (and not a retweet from someone else)?
   *
   * @param   {Object} tweet        tweet data + metadata
   * @returns {Boolean}
   */
  isCoinOfTheDay(tweet) {
    if (tweet.user.id_str == this.mcafeeId) {
      if (tweet.text.match(/coin of the day/i)) {
        return true;
      }
      return false;
    } else {
      return false;
    }
  }

  /**
   * Guess what coin he's talking about.
   *
   * @param   {Object} tweet        tweet data + metadata
   * @returns {Object}              coin metadata
   */
  identifyCoin(tweet) {
    const text = tweet.text
    console.warn(text)
    if (text.match(/Coin of the day/i)) {
      const match = text.match(/Coin of the day:\s*([\w ]*)/i)
      if (this.verbose)
        console.log(match)
      const longName = match[1].trim()
      const pattern = new RegExp(`^${longName}$`, 'i')
      const c = currencies.find((c) => {
        if (c.CurrencyLong.match(pattern)) {
          return true
        } else if (c.Currency.match(pattern)) {
          return true
        } else {
          return false;
        }
      })
      if (c) {
        return { found: true, symbol: c.Currency, name: c.CurrencyLong }
      } else {
        return { found: false, reason: `Couldn't find ${longName} in currencies list.` }
      }
    } else {
      return { found: false, reason: 'Tweet could not be parsed.' }
    }
  }

  /**
   * Buy a coin
   *
   * @param   {String} symbol       symbol for coin
   * @param   {Number} btcSpend     maximum BTC you are willing to spend
   * @param   {Number} adjustment   multiplier to apply to price to get ahead of the pump
   * @returns {Object}              result of bittrex.tradebuy
   */
  async buy(symbol, btcSpend, adjustment) {
    const market   = `BTC-${symbol}`
    const res0     = await this.bittrex.gettickerAsync({ market })
    const ticker   = res0.result
    const rate     = ticker.Ask + (ticker.Ask * adjustment)
    const quantity = btcSpend / rate
    const opts = {
      MarketName:    market,
      OrderType:     'LIMIT',
      Quantity:      quantity,
      Rate:          rate,
      TimeInEffect:  'IMMEDIATE_OR_CANCEL',
      ConditionType: 'NONE',
      Target:        0
    }
    const res1 = await this.bittrex.tradebuyAsync(opts)
    if (res1.success == 'true') {
      console.log("buy confirmed!")
    }
    return res1
  }

  async sell(market_bought, quantity, adjustment) {
    const market   = market_bought
    const res0     = await this.bittrex.gettickerAsync({ market })
    const ticker   = res0.result
    const rate     = ticker.Bid - (ticker.Bid * adjustment)
    const opts = {
      MarketName:    market,
      OrderType:     'LIMIT',
      Quantity:      quantity,
      Rate:          rate,
      TimeInEffect:  'IMMEDIATE_OR_CANCEL',
      ConditionType: 'NONE',
      Target:        0
    }
    const res1 = await this.bittrex.tradesellAsync(opts)
    if (res1.success == 'true') {
      console.log("sell confirmed!")
    } 
    return res1
  }

}